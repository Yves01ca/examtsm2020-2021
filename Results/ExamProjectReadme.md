# README #

This README is the normal document explaining whatever steps are necessary to get the application up and running.

# PYTHON EXAM PROJECT
**Exam project 2021 M2 FIT TSM-Yves ATTIPOE

This Python project was conducted in the class framework of Financial computing with Mr. Gert DE Geyter.
The objective is to build a financial model of three types of investors and complete the simulation of their investments.

## Getting Started
This guide is to help in efficiently understanding, manipulating and running the code of this project

### Prerequisites and installing
Make sure that the import packages are install in python before importing them and running the py file.
In order to efficiently run the code it is essential to have the requiered packages imported.
some packages require to be installed such as 'investpy' and 'pandas_datareader' 

```
 In the Python terminal by typing: "pip install investpy" then pressing ENTER
In the Python terminal by typing: "pip install pandas_datareader" then pressing ENTER
```

And the here is the list of all the packages required to run all the code. 

```
import matplotlib.pyplot as plt
import numpy as np
import random
import investpy
from random import choice
import math
import pandas as pd
import pandas_datareader.data as web
import datetime as dt
from datetime import date
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

* This project is divided in 4 parts where the part1 is composed of two .py files (Part1 and Part1_1suite)
* Outputs such as graphs are saved in the folder "Results" in Bitbucket
* The plots result are saved ind Result file 

### Break down into end to end tests
In running the code please be aware of the part1 and Part2 that require integer imputs from the user in the Run.

for example:
```
Enter the amount of your investment
```

### And coding style tests
The code is written in the pep8 coding style. 

## Authors

* **Yves ATTIPOE** - *Bitbucket* https://bitbucket.org/Yves01ca/

Meichen Liu also participated in this project.

## Acknowledgments

*  Meichen LIU
* Mr Gert De Geyter 

