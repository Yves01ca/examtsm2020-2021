#plot the means for every year in the given dataset (always going from 01/01 to 31/12)
#to answer this question we need to firstly comput the number of business days (for aggressive investors)and number of real days (for defensive investors).
#import the packages
import matplotlib.pyplot as plt
import numpy as np
import random
import investpy
from random import choice
import math
import pandas as pd
import pandas_datareader.data as web
import datetime as dt
from datetime import date

from Code_Part_3 import aggressive_table
from Code_Part_3 import defensive_table
from Code_Part_3 import mixed_table
from Code_Part_3 import s
from Code_Part_3 import rs
from Code_Part_3 import l
from Code_Part_3 import rl
from Code_Part_3 import risk_free
from Code_Part_3 import starting_stock_price
from Code_Part_3 import random_nb1
from Code_Part_3 import random_mixed_nb2
from Code_Part_3 import matrix_of_stocks

#with python, there exist busday which is aimed for count the business days
#however it can not allowd us to exclude holidays etc thus,here we directly use outside working days caculator to obtain the exact number
#the business day in 2016
Bdays2016 = 85
#the business day in 2017
Bdays2017 = 251
#the business day in 2018
Bdays2018 = 252
#the business day in 2019
Bdays2019 = 251
#the business day in 2020
Bdays2020 = 253
#Verification if the total business days is 1092
print(Bdays2020+Bdays2019+Bdays2018+Bdays2017+Bdays2016)

#the real day in 2016
Rstart2016 = dt.date( 2016, 9, 1 )
Rend2016 = dt.date( 2016, 12, 31 )
Rdays2016 = (Rend2016-Rstart2016).days+1
print(Rdays2016)
#the real day in 2017
Rstart2017 = dt.date( 2017, 1, 1 )
Rend2017 = dt.date( 2017, 12, 31 )
Rdays2017 = (Rend2017-Rstart2017).days+1
print(Rdays2017)
#the real day in 2018
Rstart2018 = dt.date( 2018, 1, 1 )
Rend2018 = dt.date( 2018, 12, 31 )
Rdays2018 = (Rend2018-Rstart2018).days+1
print(Rdays2018)
#the real day in 2019
Rstart2019 = dt.date( 2019, 1, 1 )
Rend2019 = dt.date( 2019, 12, 31 )
Rdays2019 = (Rend2019-Rstart2019).days+1
print(Rdays2019)
#the business day in 2020
Rstart2020 = dt.date( 2020, 1, 1 )
Rend2020 = dt.date( 2020, 12, 31 )
Rdays2020 = (Rend2020-Rstart2020).days+1
print(Rdays2020)


##return of aggressive investors
#sum of 500 aggressive investors by days
aggressive_sum = np.sum(aggressive_table, axis=0)
print(aggressive_sum)

#mean in 2016
aggressive_sum2016=aggressive_sum[0:Bdays2016-1]
mean_aggressive2016=np.mean(aggressive_sum2016)/500
print("The mean of 500 aggressive investors in 2016 is $",mean_aggressive2016)

#mean in 2017
aggressive_sum2017=aggressive_sum[Bdays2016-1:Bdays2016+Bdays2017-1]
mean_aggressive2017=np.mean(aggressive_sum2016)/500
print("The mean of 500 aggressive investors in 2017 is $",mean_aggressive2017)

#mean in 2018
aggressive_sum2018=aggressive_sum[Bdays2016+Bdays2017-1:Bdays2016+Bdays2017+Bdays2018-1]
mean_aggressive2018=np.mean(aggressive_sum2018)/500
print("The mean of 500 aggressive investors in 2018 is $",mean_aggressive2018)

#mean in 2019
aggressive_sum2019=aggressive_sum[Bdays2016+Bdays2017+Bdays2018-1:Bdays2016+Bdays2017+Bdays2018+Bdays2019-1]
mean_aggressive2019=np.mean(aggressive_sum2019)/500
print("The mean of 500 aggressive investors in 2019 is $",mean_aggressive2019)

#mean in 2020
aggressive_sum2020=aggressive_sum[Bdays2016+Bdays2017+Bdays2018+Bdays2019-1:1091]
mean_aggressive2020=np.mean(aggressive_sum2020)/500
print("The mean of 500 aggressive investors in 2020 is $",mean_aggressive2020)


##return of defensive investors
#sum of 500 defensive investors by days
defensive_sum = np.sum(defensive_table, axis=0)
print(defensive_sum)

#mean in 2016
defensive_sum2016 = defensive_sum[0:Rdays2016-1]
mean_defensive2016=np.mean(defensive_sum2016)/500
print("The mean of 500 defensive investors in 2016 is $",mean_defensive2016)

#mean in 2017
defensive_sum2017=defensive_sum[Rdays2016-1:Rdays2016+Rdays2017-1]
mean_defensive2017=np.mean(defensive_sum2016)/500
print("The mean of 500 defensive investors in 2017 is $",mean_defensive2017)

#mean in 2018
defensive_sum2018=defensive_sum[Rdays2016+Rdays2017-1:Rdays2016+Rdays2017+Rdays2018-1]
mean_defensive2018=np.mean(defensive_sum2018)/500
print("The mean of 500 defensive investors in 2018 is $",mean_defensive2018)

#mean in 2019
defensive_sum2019=defensive_sum[Rdays2016+Rdays2017+Rdays2018-1:Rdays2016+Rdays2017+Rdays2018+Rdays2019-1]
mean_defensive2019=np.mean(defensive_sum2019)/500
print("The mean of 500 defensive investors in 2019 is $",mean_defensive2019)

#mean in 2020
defensive_sum2020=defensive_sum[Rdays2016+Rdays2017+Rdays2018+Rdays2019-1:1583]
mean_defensive2020=np.mean(defensive_sum2020)/500
print("The mean of 500 defensive investors in 2020 is $",mean_defensive2020)


##return of mixed investors
#sum of 500 mixed investors by days
mixed_sum = np.sum(mixed_table, axis=0)
print(mixed_sum)

#mean in 2016
mixed_sum2016=mixed_sum[0:Bdays2016-1]
mean_mixed2016=np.mean(mixed_sum2016)/500
print("The mean of 500 mixed investors in 2016 is $",mean_mixed2016)

#mean in 2017
mixed_sum2017=mixed_sum[Bdays2016-1:Bdays2016+Bdays2017-1]
mean_mixed2017=np.mean(mixed_sum2016)/500
print("The mean of 500 mixed investors in 2017 is $",mean_mixed2017)

#mean in 2018
mixed_sum2018=mixed_sum[Bdays2016+Bdays2017-1:Bdays2016+Bdays2017+Bdays2018-1]
mean_mixed2018=np.mean(mixed_sum2018)/500
print("The mean of 500 mixed investors in 2018 is $",mean_mixed2018)

#mean in 2019
mixed_sum2019=mixed_sum[Bdays2016+Bdays2017+Bdays2018-1:Bdays2016+Bdays2017+Bdays2018+Bdays2019-1]
mean_mixed2019=np.mean(mixed_sum2019)/500
print("The mean of 500 mixed investors in 2019 is $",mean_mixed2019)

#mean in 2020
mixed_sum2020=mixed_sum[Bdays2016+Bdays2017+Bdays2018+Bdays2019-1:1091]
mean_mixed2020=np.mean(mixed_sum2020)/500
print("The mean of 500 mixed investors in 2020 is $",mean_mixed2020)

##Change mixed investors composition
#50-50
#create a table of 500 mixted investors
mixed_table1= np.zeros((500,1091))
y=0

for mixed_investor in range(1,501):
    # the money for bond is 5000*50% with this money he can buy 1 long term and 6 short term bond (same logic as before)
    beginning_mixed_bond_investment1 = (6 * s) * (1 + rs) ** 2+(1*l)*(1+rl)**5
    mixed_of_bond_investment1 = [beginning_mixed_bond_investment1 * (1 + risk_free) ** (i / 365) for i in range(0,1091)]

    # We know that the investor will use 50% of $5000 and invest one stock at maximum then switch to other stock if the remaining is larger than $100
    # From caculation we know that only if the investor pick the stock 792.89/share to have an remaning of 578.44,and this difference can be canceld by buying any other stock
    # In conclusion, the investor will buy only one stock at max or buy stock 2 with other stock.
    # genreate a random integer from 0 to 6(begin with 0) which correspond to 7 stocks
    random_mixed_nb1 = random.randint(0, 6)
    # find the starting price for the random selected stocks
    starting_mixed_price_of_random_stock1 = starting_stock_price[random_mixed_nb1]
    # caculte the number of stock purchase
    nb_of_mixed_stock_purchase1 = math.floor(2500/ starting_mixed_price_of_random_stock1)
    # caculate the remainning  money after investment
    remainning_mixed_money1 = 2500 - starting_mixed_price_of_random_stock1 * nb_of_mixed_stock_purchase1

    # when the remaning money is less than $100,print
    if remainning_mixed_money1 < 100:
        print("This mixed investor purchase ", nb_of_mixed_stock_purchase1, "of stock", random_mixed_nb1, "for $",starting_mixed_price_of_random_stock1, "per share, 5 of short term bonds and 1 of long term bond")
        # compute the investment price and add it on the table
        mixed_table1[y]= matrix_of_stocks[random_mixed_nb1] * nb_of_mixed_stock_purchase1+mixed_of_bond_investment1

    else:
        # pick another stock
        random_mixed_nb2 = random.randint(0, 6)
        # find the starting price for the second random selected stocks
        starting_mixed_price_of_random_stock2 = starting_stock_price[random_mixed_nb2]
        # caculte the number of the second tstock purchase
        nb_of_mixed_stock_purchase2 = int(remainning_mixed_money1 / starting_mixed_price_of_random_stock2)
        remainning_mixed_money2 = remainning_mixed_money1 - starting_mixed_price_of_random_stock2 * nb_of_mixed_stock_purchase2
        print("This mixed investor purchase ", nb_of_mixed_stock_purchase1, "of stock", random_mixed_nb1, "for",
              starting_mixed_price_of_random_stock1, "per share, and", nb_of_mixed_stock_purchase2, "of stock", random_mixed_nb2, "for $",
              starting_mixed_price_of_random_stock2,"per share 5 of short term bonds and 1 of long term bond")

        # compute the mixed investment price and add it on the table
        mixed_table1[y] = matrix_of_stocks[random_mixed_nb1] * nb_of_mixed_stock_purchase1 + matrix_of_stocks[random_mixed_nb2] * nb_of_mixed_stock_purchase2+mixed_of_bond_investment1

y=y+1
print(mixed_table1)
# Plot of 500 aggressive investors
plt.plot(mixed_table1, linestyle="-", label='aggressive investor')
plt.title('Plot of 500 mixed investors 50-50 bond stock from 01/09/2016 to 01/01/2021')
plt.xlabel('Number of business days start from 01/09/2016')
plt.ylabel('Price of financial instruments')
plt.show()


#75-25

#create a table of 500 mixted investors
mixed_table2= np.zeros((500,1091))
x=0
for mixed_investor in range(1,501):
    # the money for bond is 5000*75% with this money he can buy 1 long term and 11 short term bond (same logic as before)
    beginning_mixed_bond_investment2 = (11 * s) * (1 + rs) ** 2+(1*l)*(1+rl)**5
    mixed_of_bond_investment2 = [beginning_mixed_bond_investment2 * (1 + risk_free) ** (i / 365) for i in range(0,1091)]

    # We know that the investor will use 25% of $5000 and invest one stock at maximum then switch to other stock if the remaining is larger than $100
    # From caculation we know that only if the investor pick the stock 792.89/share to have an remaning of 578.44,and this difference can be canceld by buying any other stock
    # In conclusion, the investor will buy only one stock at max or buy stock 2 with other stock.
    # genreate a random integer from 0 to 6(begin with 0) which correspond to 7 stocks
    random_mixed_nb3 = random.randint(0, 6)
    # find the starting price for the random selected stocks
    starting_mixed_price_of_random_stock3 = starting_stock_price[random_mixed_nb3]
    # caculte the number of stock purchase
    nb_of_mixed_stock_purchase3 = math.floor(1250/ starting_mixed_price_of_random_stock3)
    # caculate the remainning  money after investment
    remainning_mixed_money3 = 1250 - starting_mixed_price_of_random_stock3 * nb_of_mixed_stock_purchase3

    # when the remaning money is less than $100,print
    if remainning_mixed_money3 < 100:
        print("This mixed investor purchase ", nb_of_mixed_stock_purchase3, "of stock", random_mixed_nb3, "for $",starting_mixed_price_of_random_stock3, "per share, 11 of short term bonds and 1 of long term bond")
        # compute the investment price and add it on the table
        mixed_table2[x]= matrix_of_stocks[random_mixed_nb3] * nb_of_mixed_stock_purchase3+mixed_of_bond_investment2

    else:
        # pick another stock
        random_mixed_nb4 = random.randint(0, 6)
        # find the starting price for the second random selected stocks
        starting_mixed_price_of_random_stock4 = starting_stock_price[random_mixed_nb4]
        # caculte the number of the second tstock purchase
        nb_of_mixed_stock_purchase4 = int(remainning_mixed_money3 / starting_mixed_price_of_random_stock4)
        remainning_mixed_money4 = remainning_mixed_money3 - starting_mixed_price_of_random_stock4 * nb_of_mixed_stock_purchase4
        print("This mixed investor purchase ", nb_of_mixed_stock_purchase3, "of stock", random_mixed_nb3, "for",
              starting_mixed_price_of_random_stock3, "per share, and", nb_of_mixed_stock_purchase4, "of stock", random_mixed_nb4, "for $",
              starting_mixed_price_of_random_stock4,"per share 11 of short term bonds and 1 of long term bond")

        # compute the mixed investment price and add it on the table
        mixed_table2[x] = matrix_of_stocks[random_mixed_nb3] * nb_of_mixed_stock_purchase3 + matrix_of_stocks[random_mixed_nb4] * nb_of_mixed_stock_purchase4+mixed_of_bond_investment2

x=x+1
print(mixed_table2)
# Plot of 500 aggressive investors
plt.plot(mixed_table2, linestyle="-", label='aggressive investor')
plt.title('Plot of 500 mixed investors 75-25 bond-stock from 01/09/2016 to 01/01/2021')
plt.xlabel('Number of business days start from 01/09/2016')
plt.ylabel('Price of financial instruments')
plt.show()

#From the plot we can see that the volitility decrease when the precentage of bond increase.which means that mixed become less risky and more stable return since bond increase.

#Starting budget 10 times

#It will have magnify impact which means that the aggressive type will become more risky but with higher return once it performs well.
#Thus the defensive become more relatively stable since every denfensive investor perform the same.
# The mixed one is always in the middle as before
#finished