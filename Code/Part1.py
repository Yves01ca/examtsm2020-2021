class bonds():#CREATION OF CLASS BONDS
    def __init__(self, price, maturity):
        self.price = price
        self.maturity = maturity


class short(bonds):#CREATION OF SUBCLASS SHORT
    def FVcomputation(self):
        FV = self.price * ((1 + 0.015) ** (self.maturity))
        compounding = FV - self.price
        return FV, compounding


class long(bonds):#CREATION OF SUBCLASS LONG
    def FVcomputation(self):
        FV = self.price * ((1 + 0.03) ** (self.maturity))
        compounding = FV - self.price
        return FV, compounding

#finished