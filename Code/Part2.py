import investpy
from matplotlib import pyplot as plt
import pandas_datareader.data as web
import pandas as pd

tickers = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]  # THE LIST OF STOCK TICKERS
tickerChoice = int(input("Choose a number corresponding to a ticker \n"
                         " FDX=0 " " GOOGL=1 " " IBM=2 " " KO=3 " " MS=4 " " NOK=5 " " XOM=6 "))

print("The term of your investment (start-date and end-date)\n"
      "must be between 09/01/2016 and 01/01/2021")

start_date = str(input('Enter start date(yyyy-mm-dd): '))  # INPUT THE STARTING DATE OF INVESTMENT
end_date = str(input('Enter end date(yyyy-mm-dd): '))  # INPUT THE ENDING DATE OF INVESTMENT
amount = int(input('how much do you want to invest?'))  # IMPORTANT

stock_df = investpy.get_stock_historical_data(stock=tickers[tickerChoice],
                                              # LOADING THE DATA FROM INVESTING.COM WITH INVESTPY
                                              # The data of the stocks from 9/1/2016 to 1/1/2021
                                              country='United States',
                                              from_date='01/09/2016',
                                              to_date='01/01/2021')

# print(stock_df['High'])
stock_df.reset_index(level=0, inplace=True)  # ADD THE DATE TO MAKE IT A COLUMN
stock_df = stock_df[['Date', 'High']]  # WE CAN NOW KEEP THE DATE AND HIGH COLUMN TOGETHER
# print("THIS IS THE DATA FRAME")
# print(stock_df)

# WE ARE PRINTING THE STARTING PRICE AND THE EDNDING PRICE OF THE SELECTED STOCK
start_price = stock_df[stock_df['Date'] == start_date]
start_price = start_price['High'].values[0]  # HERE WE WILL AVOID THE PRINTING OF DESCRIPTION OF DATA
print("Price of " + tickers[tickerChoice] + " on the " + str(start_date) + " is " + str(start_price))

end_price = stock_df[stock_df['Date'] == end_date]
end_price = end_price['High'].values[0]
# print(end_price)
print("Price of " + tickers[tickerChoice] + " on the " + str(end_date) + " is " + str(end_price))

# THE NUMBER OF STOCKS BOUGHT FORMULA
no_stocks = int(amount / start_price)
print("The Total number of stocks bought is  " + str(no_stocks))

# THE RETURN ON INVESTMENT FORMULA
roi = round((((end_price - start_price) * no_stocks) / start_price), 3)
FValue =  amount * (1 + roi/100)
print('Your return on investment is ' + str(roi) + ' % ' + "which is " + str(FValue))

# PLOTTING THE PORTOFOLIO OF ALL THE STOCKS WITH A NORMALIZATION THECHNIQUE
tickers = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]
dataframe = pd.DataFrame()
for t in tickers:
    dataframe[t] = web.DataReader(t, data_source="yahoo", start="2016-9-1", end="2021-1-1")["High"]
# PRINT TO OBTAIN THE FIRST ROW OF THE ENTIRE DATA
# print(dataframe.iloc[0])  # TO SEE THE STARTING FIRST HIGH PRICE OF ALL THE STOCKS
plt.plot(dataframe / dataframe.iloc[0] * 100)
plt.grid(True)
plt.title('plot of all stocks for the entire period')
plt.xlabel('Years')
plt.ylabel('price of stocks')
plt.show()

#finished